//! An iterator trait.
//!
//! ---
//! author: Andrew Evans
//! ---


use crate::records::record::Record;
use crate::records::store::RecordStore;

/// An Iterator works with the concept of a stack.
pub trait RecordIterator{
    fn has_next(&self) -> bool;
    fn next(&mut self) -> Option<Record>;
    fn collect(&mut self) -> Vec<Record>;
    fn zip(&mut self, it: &mut RecordStore) -> &mut RecordStore;
}
