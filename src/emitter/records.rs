//! Emitter for the records
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use crossbeam::crossbeam_channel::Sender;
use tokio::sync::Semaphore;

use crate::records::record::Record;
use crate::records::store::RecordStore;
use crate::stream::stream::Stream;

/// Record Emitter
pub struct RecordEmitter{
    storage: RecordStore,
    sema: Arc<Semaphore>,
    receiver_sema: Arc<Semaphore>,
    sender: Arc<Sender<(Vec<Record>, Vec<Stream>)>>,
    batch_size: usize,
    streams: Vec<Stream>
}


impl RecordEmitter{

    /// Publish records to the emitter.
    ///
    /// # Arguments
    /// * `record` - Record to publish to the store
    pub(in crate) fn publish(&mut self, record: Record){
        self.storage.store(record);
    }

    /// Emit the records
    ///
    /// # Arguments
    /// * `records` - The records to emit
    pub(in crate) async fn emit_records(&mut self) -> usize{
        let sema = self.sema.clone();
        let rsema = self.receiver_sema.clone();
        if self.storage.size().clone() > 0 {
            let mut nopt = self.storage.get_next_record();
            let mut records = vec![];
            while nopt.is_some(){
                let record = nopt.unwrap();
                records.push(record);
                nopt = self.storage.get_next_record();
            }
            let record_size = records.len().clone();
            let cfg = self.streams.clone();
            let permit = sema.acquire().await;
            permit.forget();
            let _r = self.sender.send((records, cfg));
            rsema.add_permits(1);
            record_size
        }else{
            0
        }
    }

    /// Create a new record emitter
    ///
    /// # Arguments
    /// * `batch_size` - Size of the batch to emit
    /// * `sema` - Semaphore to wait for when sending batches
    /// * `receiver_sema` - Semaphore for the receiver
    /// * `sender` - Crossbeam mpmc sender controlled by the semaphore
    /// * `flow_config` - Flow for the emitter
    pub(in crate) fn new(
        batch_size: usize,
        sema: Arc<Semaphore>,
        receiver_sema: Arc<Semaphore>,
        sender: Arc<Sender<(Vec<Record>, Vec<Stream>)>>,
        streams: Vec<Stream>) -> RecordEmitter{
        RecordEmitter{
            storage: RecordStore::new(),
            sema,
            receiver_sema,
            sender,
            batch_size,
            streams
        }
    }
}

#[cfg(test)]
pub mod tests{
    use super::*;
    use crossbeam::crossbeam_channel;
    use tokio::runtime::{Builder, Runtime};
    use tokio::sync::Semaphore;
    use crate::stream::stream::Stream;
    use std::collections::HashMap;
    use datacannon_rs_core::argparse::argtype::ArgType;

    fn get_runtime() -> Runtime{
        Builder::default()
            .threaded_scheduler()
            .core_threads(4)
            .enable_all()
            .build()
            .unwrap()
    }

    #[test]
    fn should_publish_record(){
        let mut rt = get_runtime();
        rt.block_on(async move {
            let cfg = vec!{Stream::new()};
            let sema = Arc::new(Semaphore::new(10));
            let rsema = Arc::new(Semaphore::new(10));
            let (sender, _receiver) = crossbeam_channel::bounded(10);
            let arc_sender = Arc::new(sender);
            let mut emitter = RecordEmitter::new(
                10, sema, rsema, arc_sender, cfg);
            let mut record = HashMap::new();
            record.insert("test".to_string(), ArgType::Bool(true));
            let _r = emitter.publish(record);
        });
    }

    #[test]
    fn should_emit_records(){
        let mut rt = get_runtime();
        let (sender, receiver) = crossbeam_channel::bounded(10);
        let arc_sender = Arc::new(sender);
        let rsema = Arc::new(Semaphore::new(0));
        let arc_rsema = rsema.clone();
        rt.block_on(async move {
            let handle = tokio::spawn(async move {
                let cfg = vec!{Stream::new()};
                let sema = Arc::new(Semaphore::new(10));
                let mut emitter = RecordEmitter::new(
                    10, sema, arc_rsema, arc_sender, cfg);
                let mut record = HashMap::new();
                record.insert("test".to_string(), ArgType::Bool(true));
                let _r = emitter.publish(record.clone());
                let _r = emitter.publish(record.clone());
                let n = emitter.emit_records().await;
                assert_eq!(n, 2);
            });
            let permit = rsema.acquire().await;
            permit.forget();
            let receive_result = receiver.recv();
            let (records, _stream) = receive_result.ok().unwrap();
            assert!(records.len() == 2);
        });
    }
}
