//! Link configuration. Links take records back and perform
//! special processing. This includes deduplication and splitting
//! streams into different components.
//!
//! ---
//! author: Andrew Evans
//! ----

use serde::{Serialize, Deserialize};

#[derive(Clone, Serialize, Deserialize)]
pub enum Link{
    Flow,
    DEDUPLICATION,
    SPLITTER,
    LINK,
    TERMINATE
}
