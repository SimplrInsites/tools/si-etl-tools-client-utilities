//! Stream configuration
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::{Deserialize, Serialize};
use crate::stream::link::Link;
use crate::stream::flow::source::config::Source;
use crate::stream::flow::sink::config::Sink;

/// Stream Queue
#[derive(Clone, Serialize, Deserialize)]
pub struct Stream{
    sources: Vec<Source>,
    task_queue: Vec<Link>,
    sinks: Vec<Sink>
}


impl Stream{

    /// Get the sources for the application
    pub fn get_sources(&mut self) -> Vec<Source>{
        self.sources.clone()
    }

    /// Get sinks for the application
    pub fn get_sinks(&mut self) -> Vec<Sink>{
        self.sinks.clone()
    }

    /// Add a sink to the stream
    ///
    /// # Arguments
    /// * `sink` - The sink to add
    pub fn add_sink(&mut self, sink: Sink) {
        self.sinks.push(sink);
    }

    /// Dequeue the top link in the chain.
    pub fn dequeue_link(&mut self) -> Option<Link>{
        self.task_queue.pop()
    }

    /// Enqueue a link in the stream queue
    ///
    /// # Arguments
    /// * `link` - The next link in the chain.
    pub fn enqueue_link(&mut self, link: Link){
        self.task_queue.push(link);
    }

    /// Create a new stream configuration
    pub fn new() -> Stream{
        Stream{
            sources: vec![],
            task_queue: vec![],
            sinks: vec![]
        }
    }
}
