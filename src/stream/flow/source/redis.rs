//! Redis source configuration
//!
//! Used to set up a redis pub sub source. The source receives
//! records over a specific uri and emits them with the attached `streams`
//!
//! Connections are stored in the encrypted database provided to the program.
//! One or more `connection_names` is provided. You will be prompted for the secret
//! key to unlock the database at startup. A PUBSUB is established per connection.
//!
//! Your connection needs to contain a `topic_name` in the options as well
//! as a `host` and a `port`. If `is_protected` is false, no authentication
//! is provided.
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::{Deserialize, Serialize};

use crate::stream::stream::Stream;
use std::collections::HashMap;

/// Redis Source Configuration
#[derive(Clone, Serialize, Deserialize)]
pub struct RedisSourceConfig{
    connection_names: Vec<String>,
    topics: HashMap<String, String>,
    streams: Vec<Stream>,
    batch_size: usize
}


impl RedisSourceConfig{

    /// Name of the key holding the connection information in the credentials database
    pub(in crate) fn get_connection_names(&self) -> Vec<String>{
        self.connection_names.clone()
    }

    /// Get the streams for the batches
    pub(in crate) fn get_streams(&self) -> Vec<Stream>{
        self.streams.clone()
    }

    /// Get the batch size
    pub(in crate) fn get_batch_size(&self) -> usize{
        self.batch_size.clone()
    }

    /// Batch size for the record emitter
    ///
    /// # Arguments
    /// * `batch_size` - The batch size
    pub fn batch_size(&mut self, batch_size: usize) -> &mut Self{
        self.batch_size = batch_size;
        self
    }

    /// Add a stream
    ///
    /// # Arguments
    /// * `stream` - Stream for the application
    pub fn add_stream(&mut self, stream: Stream) -> &mut Self{
        self.streams.push(stream);
        self
    }

    /// Extend the streams
    ///
    /// # Arguments
    /// * `streams` - A vector containing the streams to add
    pub fn extend_streams(&mut self, streams: Vec<Stream>) -> &mut Self{
        self.streams.extend(streams);
        self
    }

    /// Connection name for Redis
    ///
    /// # Arguments
    /// * `connection_name` - The connection name
    pub fn add_connection_name(&mut self, connection_name: String) -> &mut Self{
        self.connection_names.push(connection_name);
        self
    }

    /// Set the topic for the connection.
    ///
    /// # Arguments
    /// * `connection_name` - Name of the connection
    /// * `topic_name` - Name of the topic
    pub fn set_topic_for_connection(
        &mut self, connection_name: String, topic_name: String) -> &mut Self{
        self.topics.insert(connection_name, topic_name);
        self
    }

    /// Get the topic
    ///
    /// # Arguments
    /// * `connection_name` - Nameof the connection
    pub(in crate) fn get_topic(&self, connection_name: String) -> Option<String>{
        let opt = self.topics.get(connection_name.as_str());
        if opt.is_some(){
            let str = opt.unwrap().clone();
            Some(str)
        }else{
            None
        }
    }

    /// Create a new Redis configuration for receiving records
    pub fn new() -> RedisSourceConfig{
        RedisSourceConfig{
            connection_names: vec![],
            streams: vec![],
            batch_size: 0,
            topics: HashMap::new()
        }
    }
}
