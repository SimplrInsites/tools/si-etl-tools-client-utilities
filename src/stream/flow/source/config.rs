//! A generic source configuration
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::{Deserialize, Serialize};
use crate::stream::flow::source::file::FileSourceConfig;

#[derive(Clone, Serialize, Deserialize)]
pub enum Source{
    POSTGRESQL,
    FILE(FileSourceConfig),
    JSONL,
    CSV,
    MONGODB,
    ARANGODB,
    FIREBASE,
    REDIS
}
