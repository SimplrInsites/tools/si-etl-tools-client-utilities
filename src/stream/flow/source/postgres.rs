//! PostgreSQL Source configuration. Contains the connection name.
//!
//! The configuration does not directly contain credentials and connection.
//! You will be prompted for a credential store key on application startup.
//! This points to your credential database using the `si-credential-database`
//! tool. Store credentials, the `uri` and `port` in this system.
//!
//! Other variables contained in the configuration include the `num_connections`
//! (number of connections), `batch_size` (batch_size), and `streams` (vector
//! of streams to output batches to).
//!
//! Connections and queries run asynchronously with the support of the
//! `tokio_postgres` crate.
//!
//! A separate source, `postgres_stream`, supports the Postgres Notify channel.
//!
//! The source expects data to contain a queryable `id_field`. This id can be
//! queried from an offset. If a `processed_flag` field is specified, this is checked
//! for a count of expected records before proceeding. The lowest number id is also
//! obtained to limit the number of queries run.
//!
//! Queries should incorporate your `processed_flag` field check as they are
//! used to grab an initial expected count.
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::stream::stream::Stream;

/// Postgresql Source configuration
pub struct PostgresSourceConfig{
    connection_name: String,
    query_string: String,
    processed_flag: String,
    id_field: String,
    streams: Vec<Stream>
}


impl PostgresSourceConfig{

    /// Get the stream vector
    pub(in crate) fn get_streams(&self) -> Vec<Stream>{
        self.streams.clone()
    }

    /// Get the id field
    pub(in crate) fn get_id_field(&self) -> String{
        self.id_field.clone()
    }

    /// Get the processed flag field name
    pub(in crate) fn get_processed_flag(&self) -> String{
        self.processed_flag.clone()
    }

    /// Get the query string
    pub(in crate) fn get_query_string(&self) -> String{
        self.query_string.clone()
    }

    /// Get the connection name
    pub(in crate) fn get_connection_name(&self) -> String{
        self.connection_name.clone()
    }

    /// Id field for the count query.
    ///
    /// # Arguments
    /// * `id_field` - The id field for the operation.
    pub fn id_field(&mut self, id_field: String) -> &mut Self{
        self.id_field = id_field;
        self
    }

    /// Set the processed flag field for the count query.
    ///
    /// # Arguments
    /// * `flag_field` - Field to check against in the pull
    pub fn processed_flag(&mut self, flag_field: String) -> &mut Self{
        self.processed_flag = flag_field;
        self
    }

    /// Set the query string for the operation
    ///
    /// # Arguments
    /// * `query_string` - Query string for the select
    pub fn query_string(&mut self, query_string: String) -> &mut Self{
        self.query_string = query_string;
        self
    }

    /// Extend streams for the batches
    ///
    /// # Arguments
    /// * `streams` - Streams to use in processing the batches
    pub fn extend_streams(&mut self, streams: Vec<Stream>) -> &mut Self{
        self.streams.extend(streams);
        self
    }

    /// Add a stream to for the resulting batches
    ///
    /// # Arguments
    /// * `streams` - A stream to add
    pub fn add_stream(&mut self, stream: Stream) -> &mut Self{
        self.streams.push(stream);
        self
    }

    /// Set the name of the connection from the encrypted store.
    ///
    /// # Arguments
    /// * `connection_name` - Name of the SQL connection
    pub fn set_connection_name(&mut self, connection_name: String) -> &mut Self{
        self.connection_name = connection_name;
        self
    }

    /// Create a new configuration with default values
    pub fn new() -> PostgresSourceConfig{
        PostgresSourceConfig{
            connection_name: "".to_string(),
            query_string: "".to_string(),
            processed_flag: "".to_string(),
            id_field: "".to_string(),
            streams: vec![]
        }
    }
}
