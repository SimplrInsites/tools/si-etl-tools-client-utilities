//! CSV Source configuration. Provide directories and files, streams, the maximum number
//! of open files allowed, batch size, and CSV reader options.
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::{Serialize, Deserialize};
use si_file_utils::csv::csvreader::CSVReaderOptions;

use crate::stream::stream::Stream;

/// CSV Source configuration
#[derive(Clone, Serialize, Deserialize)]
pub struct CSVSourceConfig{
    reader_options: CSVReaderOptions,
    paths: Vec<String>,
    streams: Vec<Stream>,
    max_open_files: usize,
    batch_size: usize
}


impl CSVSourceConfig{

    /// Get the CSV Reader Options
    pub fn get_reader_options(&self) -> CSVReaderOptions{
        self.reader_options.clone()
    }

    /// Get the batch size
    pub(in crate) fn get_batch_size(&self) -> usize{
        self.batch_size.clone()
    }

    /// Get the streams eminating from the source
    pub(in crate) fn get_streams(&self) -> Vec<Stream>{
        self.streams.clone()
    }

    /// Get the maximum number of open files
    pub(in crate) fn get_max_open_files(&self) -> usize{
        self.max_open_files.clone()
    }

    /// Get file paths
    pub(in crate) fn get_paths(&self) -> Vec<String>{
        self.paths.clone()
    }

    /// Add a path to the paths
    ///
    /// # Arguments
    /// * `path` - Directory path add
    pub fn add_path(&mut self, path: String) -> &mut Self{
        self.paths.push(path);
        self
    }

    /// Set the batch size
    ///
    /// # Arguments
    /// * `batch_size` - Size of the batch to read
    pub fn batch_size(&mut self, batch_size: usize) -> &mut Self{
        self.batch_size = batch_size;
        self
    }

    /// Add a stream to the output for the source
    ///
    /// # Arguments
    /// * `stream` - Stream to add
    pub fn add_stream(&mut self, stream: Stream) -> &mut Self{
        self.streams.push(stream);
        self
    }

    /// Add all streams to the current streams list
    ///
    /// # Arguments
    /// * `streams` - Streams for the source
    pub fn extend_streams(&mut self, streams: Vec<Stream>) -> &mut Self{
        self.streams.extend(streams);
        self
    }

    /// Maximum number of files to open concurrently
    ///
    /// # Arguments
    /// * `max` - Maximum number of open files
    pub fn set_max_open_files(&mut self, max: usize) -> &mut Self{
        self.max_open_files = max;
        self
    }

    /// Create a new file source config
    ///
    /// # Arguments
    /// * `reader_options` - CSV options including delimiters and headers
    pub fn new(reader_options: CSVReaderOptions) -> CSVSourceConfig{
        CSVSourceConfig{
            reader_options,
            paths: vec![],
            streams: vec![],
            max_open_files: 1,
            batch_size: 0
        }
    }
}
