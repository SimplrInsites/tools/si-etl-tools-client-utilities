//! A generic sink configuration
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::{Serialize, Deserialize};

#[derive(Clone, Serialize, Deserialize)]
pub enum Sink{
    POSTGRESQL,
    ARANGODB,
    FIREBASE,
    MONGODB,
    CSV,
    JSONL,
    REDIS
}
