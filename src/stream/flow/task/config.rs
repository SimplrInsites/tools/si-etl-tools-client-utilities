//! A generic FlowTaskConfig
//!
//! ---
//! author: Andrew Evans
//! ---

use datacannon_rs_core::message_protocol::stream::StreamConfig;
use serde::{Deserialize, Serialize};

/// Flow task configuration
#[derive(Clone, Serialize, Deserialize)]
pub struct FlowTaskConfig{
    stream: StreamConfig
}


impl FlowTaskConfig{

    /// Create a new task flow config
    pub fn new() -> FlowTaskConfig{
        let stream = StreamConfig::new(None, None);
        FlowTaskConfig{
            stream
        }
    }
}


#[cfg(test)]
pub mod tests{

    #[test]
    fn should_create_task_config(){

    }
}