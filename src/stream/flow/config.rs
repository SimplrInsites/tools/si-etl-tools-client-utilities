//! Flow configuration. The task is an option to allow for source to sink.
//! The sink is a task and will be appended once your chains are complete.
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::{Deserialize, Serialize};
use crate::stream::flow::source::config::Source;
use crate::stream::flow::task::config::FlowTaskConfig;
use crate::stream::flow::sink::config::Sink;

/// A FlowConfigBuilder for the application
#[derive(Clone, Serialize, Deserialize)]
pub struct FlowConfig{
    pub tasks: FlowTaskConfig,
    pub sources: Vec<Source>,
    pub sinks: Option<Vec<Sink>>,
}


/// Implementation of the flow config builder
impl FlowConfig{

    /// Add a source to the flow
    ///
    /// # Arguments
    /// * `source` - Source configuration
    pub fn add_source(&mut self, source: Source) -> &mut Self{
        self.sources.push(source);
        self
    }

    /// Add a sink to the flow
    ///
    /// # Arguments
    /// * `sink` - Sink configuration
    pub fn add_sink(&mut self, sink: Sink) -> &mut Self{
        if self.sinks.is_none(){
            self.sinks = Some(vec![]);
        }
        let mut sinks_vec = self.sinks.clone().unwrap();
        sinks_vec.push(sink);
        self.sinks = Some(sinks_vec);
        self
    }

    /// Set the tasks
    ///
    /// # Arguments
    /// * `config` - The flow task config
    pub fn set_tasks(&mut self, config: FlowTaskConfig) -> &mut Self{
        self.tasks = config;
        self
    }

    /// Get a FlowConfigBuilder with Defaults
    pub fn default() -> FlowConfig{
        FlowConfig{
            tasks: FlowTaskConfig::new(),
            sources: vec![],
            sinks: None
        }
    }
}
