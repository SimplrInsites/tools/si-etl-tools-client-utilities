//! Application configuration
//!
//! ---
//! author: Andrew Evans
//! ---

use datacannon_rs_client::app::config::GeneralConfig;
use datacannon_rs_core::backend::types::AvailableBackend;
use log::LevelFilter;
use datacannon_rs_core::config::config::BrokerType;

/// ETL Application configuration builder
pub struct ETLAppConfigBuilder{
    general_config: GeneralConfig,
    backend: AvailableBackend,
    log_level_filter: Option<LevelFilter>,
    broker_type: Option<BrokerType>
}

/// ETL application config
pub struct  ETLAppConfig{
    pub general_config: GeneralConfig,
    pub backend: AvailableBackend,
    pub log_level_filter: Option<LevelFilter>,
    pub broker_type: BrokerType
}

impl ETLAppConfigBuilder{

    pub fn broker_type(&mut self, broker_type: BrokerType) -> &mut Self{
        self.broker_type = Some(broker_type);
        self
    }

    /// Set the backend
    ///
    /// # Arguments
    /// * `backend` - The backend to use
    pub fn set_backend(&mut self, backend: AvailableBackend) -> &mut Self{
        self.backend = backend;
        self
    }

    /// Set the general config
    ///
    /// # Arguments
    /// * `conifg` - The general configuration
    pub fn set_general_config(&mut self, config: GeneralConfig) -> &mut Self{
        self.general_config = config;
        self
    }

    /// Set the level filter
    ///
    /// # Arguments
    /// * `level_filter` - Logging level filter
    pub fn set_log_level_filter(&mut self, level_filter: Option<LevelFilter>) -> &mut Self{
        self.log_level_filter = level_filter;
        self
    }

    /// Get the default builder
    pub fn default() -> ETLAppConfigBuilder{
        ETLAppConfigBuilder{
            general_config: GeneralConfig::new(),
            backend: AvailableBackend::NONE,
            log_level_filter: None,
            broker_type: None
        }
    }

    /// Build the config, consuming the builder
    pub fn build(self) -> ETLAppConfig{
        if self.broker_type.is_none(){
            panic!("Broker type not specified!");
        }
        ETLAppConfig{
            general_config: self.general_config,
            backend: self.backend,
            log_level_filter: self.log_level_filter,
            broker_type: self.broker_type.unwrap()
        }
    }
}
