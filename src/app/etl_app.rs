//! ETL application. A proxy for a load balanced set of other applications. This is a proxy
//! that submits tasks to the pool based applications.
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;

use datacannon_rs_client::app::app::{App, AppBuilder};
use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::config::config::CannonConfig;
use datacannon_rs_core::result::asynchronous::AsyncResult;
use datacannon_rs_core::result::callbacks::callback::Callback;
use datacannon_rs_core::result::callbacks::errback::Errback;
use datacannon_rs_core::task::config::{TaskConfig, TaskConfigBuilder};
use si_credentials_store::registry::credentials::store::CredentialDatabase;
use tokio::sync::RwLock;
use tokio::task::JoinHandle;

use crate::app::config::app_config::ETLAppConfig;
use crate::stream::stream::Stream;

/// ETL Application
pub struct ETLApp{
    status: Arc<AtomicBool>,
    stream_monitor: Option<JoinHandle<()>>,
    app: App,
    credential_store: Arc<RwLock<CredentialDatabase>>
}

impl ETLApp{

    /// Send an task to the application.
    ///
    /// # Arguments
    /// * `stream` - Flow based configuration
    /// * `errbacks` - Errbacks for tasks
    /// * `callbacks` - Callbacks for tasks
    pub async fn stream(
        &mut self,
        stream: Stream,
        errbacks: Option<Vec<Errback>>,
        callbacks: Option<Vec<Callback>>) -> Result<AsyncResult, ()>{
        Err(())
    }

    /// Submit a single task to the worker. This is a convenience function when you
    /// do not need a stream. Since the framework is distribution for batch jobs, you
    /// should ensure any work is split to ensure
    ///
    /// # Arguments
    /// * `task` - The task configuration
    pub async fn submit_task(
        &mut self,
        task: TaskConfig,
        errbacks: Option<Vec<Errback>>,
        callbacks: Option<Vec<Callback>>) -> Result<AsyncResult, ()>{
        let r = self.app.submit_task(
            task, None, errbacks, callbacks).await;
        r
    }

    /// Create the task builder.
    ///
    /// # Arguments
    /// * `args` - Argsuments for the task
    /// * `kwargs` - Mapped arguments for the task
    /// * `expires` - Potential expiration time for the task (time when it no longer runs)
    /// * `soft_time_limit` - Soft expiration time (after started)
    /// * `time_limit` - Hard time limit for the task (kills after started)
    pub fn create_task_builder(
        &mut self, task_name: String,
        args: Option<Vec<ArgType>>,
        kwargs: Option<HashMap<String, ArgType>>,
        expires: Option<u64>,
        soft_time_limit: Option<u64>,
        time_limit: Option<u64>) -> Result<TaskConfigBuilder, ()>{
        self.app.create_task_builder(
            task_name, args, kwargs, expires, soft_time_limit, time_limit)
    }

    /// Register the task with the application. Must be done before submitting
    /// a task.
    ///
    /// # Arguments
    /// * `task_name` - Name of the task
    pub fn register_task(&mut self, task_name: &'static str){
        self.app.register_task(task_name);
    }

    /// Close the application
    pub async fn close(&self){
        self.app.close().await
    }

    /// Setup the sinks
    fn setup_sinks(&mut self){

    }

    /// Setup the sources
    fn setup_sources(&mut self){

    }

    /// Setup the monitor for the application
    fn setup_monitor(&mut self){

    }

    /// Create a new application for the pool.
    ///
    /// # Arguments
    /// * `app_name` - Application name
    /// * `notifier` - Notifier for the application
    /// * `channel` - Channel for receiving configs to send
    /// * `status` - Status of the application
    /// * `broker_type` - Type of broker to start
    /// * `backend` - Backend type
    pub async fn new(
        app_name: &'static str,
        app_config: ETLAppConfig,
        cannon_config: CannonConfig<'static>,
        credential_store: CredentialDatabase) -> Result<ETLApp, ()>{
        let mut builder = AppBuilder::default();
        builder.backend(app_config.backend);
        builder.general_config(app_config.general_config);
        builder.broker(app_config.broker_type);
        builder.app_name(app_name);
        builder.config(cannon_config);
        let app_result = builder.build(
            app_config.log_level_filter).await;
        if app_result.is_ok() {
            let app = app_result.unwrap();
            let app = ETLApp {
                status: Arc::new(AtomicBool::new(true)),
                app,
                credential_store: Arc::new(RwLock::new(credential_store)),
                stream_monitor: None
            };
            Ok(app)
        }else{
            Err(())
        }
    }
}

#[cfg(test)]
pub mod test{
    use std::env;
    use std::io::stdin;

    use datacannon_rs_client::app::config::GeneralConfig;
    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::backend::types::AvailableBackend;
    use datacannon_rs_core::config::config::{BackendType, BrokerType};
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_structure::amqp::exchange::Exchange;
    use datacannon_rs_core::message_structure::amqp::queue::AMQPQueue;
    use datacannon_rs_core::message_structure::queues::GenericQueue;
    use datacannon_rs_core::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use datacannon_rs_core::replication::replication::HAPolicy;
    use datacannon_rs_core::router::router::{Router, Routers};
    use lapin::ExchangeKind;
    use log::LevelFilter;
    use si_credentials_store::registry::credentials::builder::CredentialDatabaseBuilder;
    use tokio::runtime::{Builder, Runtime};

    use super::*;

    /// Get the runtime.
    fn get_runtime() -> Runtime{
        let r = Builder::default()
            .threaded_scheduler()
            .core_threads(2)
            .enable_all()
            .build();
        r.unwrap()
    }

    fn get_routers() -> Routers {
        let mut rts = Routers::new();
        let mut queues = Vec::<GenericQueue>::new();
        let amq_conf = AMQPConnectionInf::new("amqp", "127.0.0.1", 5672, Some("test"), Some("dev"), Some("rtp*4500"), 1000);
        let q = AMQPQueue::new(
            "lapin_test_queue".to_string(),
            Some("test_exchange".to_string()),
            Some("test_route".to_string()),
            0,
            HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(
                RabbitMQHAPolicies::ALL, 1)), false, amq_conf);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange", ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(), queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp", "127.0.0.1",
            5672,
            Some("test"),
            Some(user.ok().unwrap().as_str()),
            Some(pwd.ok().unwrap().as_str()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = get_routers();
        let mut cannon_conf = CannonConfig::new(
            conn_conf,
            BackendType::REDIS,
            backend_conf,
            routers);
        cannon_conf.num_broker_channels = 30;
        cannon_conf.consumers_per_queue = 25;
        cannon_conf.default_routing_key = "test_key";
        cannon_conf.default_exchange = "test_exchange";
        cannon_conf.default_exchange_type = ExchangeKind::Direct;
        cannon_conf
    }

    fn get_backend_config() -> BackendConfig {
        let bc = BackendConfig {
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_credential_database() -> CredentialDatabase{
        let metapath = env!("metapath");
        let vpath = env!("vpath");
        let fpath = env!("credspath");
        let std_in = stdin();
        println!("Enter the secret key");
        let mut secret = String::new();
        let r = std_in.read_line(&mut secret);
        secret = secret.trim().to_string();
        assert!(r.is_ok());
        let mut builder = CredentialDatabaseBuilder::default();
        builder.verification_path(vpath.to_string());
        builder.fpath(fpath.to_string());
        builder.meta_path(metapath.to_string());
        builder.build(secret).unwrap()
    }

    #[test]
    fn should_build_task(){
        let mut rt = get_runtime();
        rt.block_on(async move {
            let creds = get_credential_database();
            let backend_config = get_backend_config();
            let config = ETLAppConfig {
                general_config: GeneralConfig::new(),
                backend: AvailableBackend::Redis((backend_config, 2)),
                log_level_filter: Some(LevelFilter::Info),
                broker_type: BrokerType::RABBITMQ
            };
            let cfg = get_config();
            let app_result = ETLApp::new(
                "test_app",
                config,
                cfg,
                creds).await;
            let mut app = app_result.unwrap();
            let r = app.create_task_builder(
                "test_it".to_string(),
                None,
                None,
                None,
                None,
                None);
            assert!(r.is_ok());
            let task: TaskConfig = r.unwrap().build().unwrap();
            assert!(task.get_task_name().eq("test_it"));
            app.close().await;
        });
    }
}
