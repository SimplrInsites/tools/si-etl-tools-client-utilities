//! Trait for dealing with security.
//!
//! ---
//! author: Andrew Evans
//! ---


use si_credentials_store::registry::credentials::models::credential::Credential;

/// Trait for securing the application
pub trait Secure{
    fn get_credential(&mut self) -> Credential;
}
