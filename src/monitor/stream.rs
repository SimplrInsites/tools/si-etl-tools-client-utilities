//! Contains an asynchronous stream monitor that waits on and handles asynchronous signals.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::time::Duration;

use tokio::sync::mpsc::Receiver;
use tokio::time::timeout;

pub enum Signal{
    SOURCECOMPLETE,
    TERMINATE
}


pub async fn monitor(
    receiver: Receiver<Signal>, source_count: usize, status: Arc<AtomicBool>){
    let mut rx = receiver;
    let mut sc = source_count;
    while status.load(Ordering::Relaxed) {
        let signal_result = timeout(
            Duration::from_millis(10000), rx.recv()).await;
        if let Ok(signal_result) = signal_result {
            match signal_result{
                Some(Signal::SOURCECOMPLETE) =>{
                    sc -= 1;
                    if sc == 0{
                        status.store(false, Ordering::Relaxed)
                    }
                },
                Some(Signal::TERMINATE) => {
                    status.store(false, Ordering::Relaxed)
                },
                None =>{}
            }
        }
    }
}
