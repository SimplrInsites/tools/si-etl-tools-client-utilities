//! A records store.
//!
//! ---
//! author: Andrew Evans
//! ---


use crate::records::record::Record;
use crate::iterator::record::RecordIterator;

/// Record Storage structure.
#[derive(Clone, Debug)]
pub struct RecordStore{
    records: Vec<Record>
}


impl RecordStore{

    /// Collect the records
    pub(in crate) fn collect_records(&mut self) -> Vec<Record>{
        self.collect()
    }

    /// Zip two record stores together
    ///
    /// # Arguments
    /// * `it` - Record iterator to append to.
    pub(in crate) fn zip_records(&mut self, it: &mut RecordStore) -> &mut Self{
        self.zip(it)
    }

    /// Get a slice of records up to the given size.
    ///
    /// # Arguments
    /// * `size` - Number of potential records to include
    pub(in crate) fn get_slice(&mut self, size: usize) -> Vec<Record>{
        let mut records = vec![];
        while records.len() < size{
            let record = self.next();
            if record.is_some(){
                let r = record.unwrap();
                records.push(r);
            }else{
                break;
            }
        }
        records
    }

    /// Get the next record from the iterator or None
    pub(in crate) fn get_next_record(&mut self) -> Option<Record>{
        self.next()
    }

    /// Store the record in the record store.
    pub(in crate) fn store(&mut self, record: Record){
        self.records.push(record);
    }

    /// Get the number of records in the store
    pub(in crate) fn size(&self) -> usize{
        self.records.len()
    }
    
    pub(in crate) fn new() -> RecordStore{
        RecordStore{
            records: vec![]
        }
    }
}


impl RecordIterator for RecordStore{

    /// Check whether there is another record in the iterator
    fn has_next(&self) -> bool {
        if self.records.is_empty(){
            false
        }else{
            true
        }
    }

    /// Get the next record in the iterator
    fn next(&mut self) -> Option<Record> {
        if self.has_next() {
            let record = self.records.pop();
            record
        }else{
            None
        }
    }

    /// Clone all records into a new vector
    fn collect(&mut self) -> Vec<Record> {
        self.records.clone()
    }

    /// Zip two record stores. Appends records from the first
  ///
  /// # Arguments
  /// * `it` - Iterator to append from
    fn zip(&mut self, it: &mut RecordStore) -> &mut Self {
        let mut new_records = it.records.clone();
        self.records.append(&mut new_records);
        self
    }
}
