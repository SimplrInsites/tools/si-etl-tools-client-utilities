//! Readers records from PostgreSQL
//!
//! The source expects data to contain a queryable `id_field`. This id can be
//! queried from an offset. If a `processed_flag` field is specified, this is checked
//! for a count of expected records before proceeding. The lowest number id is also
//! obtained to limit the number of queries run.
//!
//! Queries limit input to the batch_size, incrementing the offset as required.
//!
//! Queries should incorporate your `processed_flag` field check as they are
//! used to grab an initial expected count.
//!
//! See the configuration for more information
//!
//! ---
//! author: Andrew Evans
//! ---

use si_credentials_store::registry::credentials::store::CredentialDatabase;
use tokio::sync::mpsc::Sender;

use crate::control::emission::EmissionControl;
use crate::monitor::stream::Signal;
use crate::sources::traits::Source;
use crate::stream::flow::source::postgres::PostgresSourceConfig;

pub struct PostgreSource{
    config: PostgresSourceConfig,
    control: EmissionControl,
    signal_channel: Sender<Signal>,
    credentials_db: CredentialDatabase
}


impl PostgreSource{

    /// Run the Postgres tool asynchronously.
    pub async fn run(&self){

    }

    /// Create a new PostgreSource
    ///
    /// # Arguments
    /// * `config` - The file source configuration
    /// * `emission_control` - Structures contriol
    /// * `signal_channel` - Sends signals for the source
    /// * `credentials_db` - Credentials database
    pub fn new(
        config: PostgresSourceConfig,
        emission_control: EmissionControl,
        signal_channel: Sender<Signal>,
        credentials_db: CredentialDatabase) -> PostgreSource{
        PostgreSource{
            config,
            control: emission_control,
            signal_channel,
            credentials_db
        }
    }
}


#[cfg(test)]
pub mod tests{

}

