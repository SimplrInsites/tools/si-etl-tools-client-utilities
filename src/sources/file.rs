//! Reads records from file sources, packages them, and sends them off. Reads multiple
//! files from a specified directory based on a regular expression.
//!
//! Reads a number of files up to the maximum amount. You shouldn't need to create
//! dozens of sources for different files. Provide directory or file paths through the
//! sources config.
//!
//! The source opens up the `max_open_files` size and reads to individual record emitters.
//! Emmitters emit records to the stream at `batch_size` or on request (when the files are
//! read).
//!
//! The source appends each line to the stream as `line` with the `path` as a separate field.
//! A `line_number` is provided as well.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::{HashMap, HashSet};
use std::sync::Arc;

use datacannon_rs_core::argparse::argtype::ArgType;
use si_file_utils::file::streamreader::{create_iterator, FileLineReader};
use tokio::sync::mpsc::Sender as TokioSender;
use tokio::sync::Mutex;

use crate::control::emission::EmissionControl;
use crate::emitter::records::RecordEmitter;
use crate::file::utilities::get_files;
use crate::monitor::stream::Signal;
use crate::stream::flow::source::file::FileSourceConfig;

/// File source implementation
pub struct FileSource{
    config: FileSourceConfig,
    control: EmissionControl,
    file_stack: Arc<Mutex<Vec<String>>>,
    signal_channel: TokioSender<Signal>
}


impl FileSource{

    /// Pops and reads files asynchronously.
    ///
    /// # Arguments
    /// * `credentials_db` - Credentials database
    /// * `file_stack` - Stack of current files
    /// * `cfg` - File Sources config
    async fn file_reader(
        file_stack: Arc<Mutex<Vec<String>>>,
        cfg: FileSourceConfig,
        ctl: EmissionControl){
        let fstack = file_stack;
        let mut num_current_records = 0;
        let batch_size = cfg.get_batch_size();
        let sender_sema = ctl.get_sender_sema();
        let receiver_sema = ctl.get_receiver_sema();
        let sender = ctl.get_sender();
        let streams = cfg.get_streams();
        let mut emitter = RecordEmitter::new(
            batch_size.clone(), sender_sema, receiver_sema, sender, streams);
        let mut rlock = fstack.lock().await;
        let mut f = (*rlock).pop();
        drop(rlock);
        while f.is_some(){
            let path: String = f.unwrap();
            let reader = FileLineReader::new(path.clone());
            let (mut read, mut it) = create_iterator(
                reader,Some(10), None, None);
            let h = tokio::spawn(async move{
               read.read_lines().await;
            });
            let mut run = true;
            let mut current_line = 0;
            while run {
                let r = it.next().await;
                let stropt = r.ok().unwrap();
                if stropt.is_none(){
                    run = false;
                }else{
                    let line = stropt.unwrap();
                    current_line += 1;
                    let mut record = HashMap::<String, ArgType>::new();
                    record.insert("line".to_string(), ArgType::String(line));
                    record.insert("path".to_string(), ArgType::String(path.clone()));
                    record.insert("line_number".to_string(), ArgType::Int(current_line));
                    emitter.publish(record);
                    num_current_records += 1;
                    if num_current_records == batch_size{
                        let _r = emitter.emit_records().await;
                        num_current_records = 0;
                    }
                }
            }
            it.close().await;
            let _r = h.await;
            rlock = fstack.lock().await;
            f = (*rlock).pop();
            drop(rlock);
        }
        let _r = emitter.emit_records().await;
    }

    /// Start the file source reader and wait for the stream to close. Pushes all discovered
    /// files once.
    pub async fn run(&self){
        let ctl = self.control.clone();
        let config = self.config.clone();
        let fstack = self.file_stack.clone();
        let paths = self.config.get_paths();
        let mut files = get_files(paths);
        files.sort();
        let fset: HashSet<String> = files.drain(..).collect();
        files = fset.into_iter().collect();
        if files.len() > 0 {
            let mut handles = vec![];
            let mut wlock = fstack.lock().await;
            (*wlock).extend(files);
            drop(wlock);
            while handles.len() < config.get_max_open_files(){
                let arc_ctl = ctl.clone();
                let arc_fstack = fstack.clone();
                let reader_cfg = config.clone();
                let handle = tokio::spawn(async move{
                    FileSource::file_reader(
                        arc_fstack,
                        reader_cfg,
                        arc_ctl).await;
                });
                handles.push(handle);
            }
            for handle in handles{
                let _r = handle.await;
            }
            let mut sch = self.signal_channel.clone();
            let _r = sch.send(Signal::SOURCECOMPLETE).await;
        }
    }

    /// Create a new file source
    /// 
    /// # Arguments
    /// * `config` - The file source configuration
    /// * `emission_control` - Structures contriol
    pub fn new(
        config: FileSourceConfig,
        emission_control: EmissionControl,
        signal_channel: TokioSender<Signal>) -> FileSource{
        FileSource{
            config,
            control: emission_control,
            file_stack: Arc::new(Mutex::new(vec![])),
            signal_channel
        }
    }
}


#[cfg(test)]
pub mod tests{
    use crossbeam::crossbeam_channel::Receiver;
    use tokio::runtime::{Builder, Runtime};
    use tokio::sync::mpsc;

    use crate::records::record::Record;
    use crate::stream::stream::Stream;

    use super::*;

    fn get_test_directory() -> String{
        let dir = env!("test_file_directory");
        let sep = std::path::MAIN_SEPARATOR;
        format!("{}{}{}", dir, sep, "text")
    }

    /// Get a single file test.txt
    fn get_test_file() -> String{
        let d = get_test_directory();
        let sep = std::path::MAIN_SEPARATOR;
        format!("{}{}{}", d, sep, "test.txt")
    }

    fn get_runtime() -> Runtime{
        let r = Builder::default()
            .threaded_scheduler()
            .core_threads(4)
            .enable_all()
            .build();
        r.unwrap()
    }

    fn get_file_config() -> FileSourceConfig{
        let fdir = get_test_directory();
        let mut fsource = FileSourceConfig::new();
        fsource.add_path(fdir);
        fsource
    }

    fn get_emission_control() -> (EmissionControl, Arc<Receiver<(Vec<Record>, Vec<Stream>)>>){
        EmissionControl::new(10)
    }

    #[test]
    fn should_read_single_file_to_completion(){
        let mut rt = get_runtime();
        let (sender, _receiver) = mpsc::channel(100);
        let dir = get_test_file();
        let (ec, mut rx) = get_emission_control();
        let mut fcfg = FileSourceConfig::new();
        fcfg.add_path(dir);
        fcfg.batch_size(100);
        fcfg.set_max_open_files(2);
        let fs = FileSource::new(fcfg, ec.clone(), sender);
        rt.block_on(async move {
            let h = tokio::spawn(async move {
                fs.run().await
            });
            let sema = ec.get_receiver_sema().clone();
            let permit = sema.acquire().await;
            permit.forget();
            let r = rx.recv();
            assert!(r.is_ok());
            let (r, _s) = r.unwrap();
            assert!(r.len() > 0);
            let _r = h.await;
        });
    }

    #[test]
    fn should_read_from_multiple_files(){
        let mut rt = get_runtime();
        let (sender, _receiver) = mpsc::channel(100);
        let dir = get_test_directory();
        let (ec,  rx) = get_emission_control();
        let mut fcfg = FileSourceConfig::new();
        fcfg.add_path(dir);
        fcfg.batch_size(100);
        fcfg.set_max_open_files(2);
        let fs = FileSource::new(fcfg, ec.clone(), sender);
        rt.block_on(async move {
            let h = tokio::spawn(async move {
                fs.run().await
            });
            for _i in 0..3 {
                let sema = ec.get_receiver_sema().clone();
                let permit = sema.acquire().await;
                permit.forget();
                let r = rx.recv();
                assert!(r.is_ok());
                let (r, s) = r.unwrap();
                assert!(r.len() > 0);
            }
            let _r = h.await;
        });
    }

    #[test]
    fn should_send_end_of_stream(){
        let mut rt = get_runtime();
        let (sender, mut receiver) = mpsc::channel(100);
        let dir = get_test_file();
        let (ec, rx) = get_emission_control();
        let mut fcfg = FileSourceConfig::new();
        fcfg.add_path(dir);
        fcfg.batch_size(100);
        fcfg.set_max_open_files(2);
        let fs = FileSource::new(fcfg, ec.clone(), sender);
        rt.block_on(async move {
            let h = tokio::spawn(async move {
                fs.run().await
            });
            let signal_opt = receiver.recv().await;
            assert!(signal_opt.is_some());
            let signal = signal_opt.unwrap();
            match signal{
                Signal::SOURCECOMPLETE =>{},
                _ => assert!(false)
            };
            let _r = h.await;
        });
    }
}
