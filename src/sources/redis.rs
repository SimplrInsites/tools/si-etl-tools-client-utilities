//! Uses the redis PUBSUB to receive records. An emitter stores the records
//! which are streamed in batch after reaching a certain number.
//!
//! See the source configuration for more information.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;
use std::net::SocketAddr;
use std::sync::Arc;

use datacannon_rs_core::argparse::argtype::ArgType;
use futures::StreamExt;
use redis_async::client;
use redis_async::resp::FromResp;
use redis_async::resp::RespValue;
use si_credentials_store::registry::credentials::models::credential::Credential;
use si_credentials_store::registry::credentials::store::CredentialDatabase;
use tokio::sync::mpsc::Sender;
use tokio::sync::Mutex;

use crate::control::emission::EmissionControl;
use crate::emitter::records::RecordEmitter;
use crate::monitor::stream::Signal;
use crate::records::record::Record;
use crate::stream::flow::source::redis::RedisSourceConfig;
use std::time::Duration;

/// Redis Source
pub struct RedisSource{
    config: RedisSourceConfig,
    control: EmissionControl,
    signal_channel: Sender<Signal>,
    credentials_db: CredentialDatabase
}


impl RedisSource{

    /// Get a credential from the database
    ///
    /// # Arguments
    /// * `connection_names` - Connection names in the application
    fn get_credentials(&mut self, connection_names: Vec<String>) -> Vec<(String, Credential)>{
        let mut output = vec![];
        for connection_name in connection_names {
            let v = self.credentials_db.get_credential(
                connection_name.as_str());
            if v.is_some(){
                let cred = v.unwrap();
                output.push((connection_name, cred.clone()));
            }
        }
        output
    }

    /// Create a consumer.
    ///
    /// # Arguments
    /// * `topic` - Topic to consume from
    /// * `host` - IP address to redis
    /// * `port` - Port for redis
    /// * `ctl` - Emission Control structure
    /// * `pwd` - Password for the consumer
    pub async fn create_consumer(
        cfg: RedisSourceConfig,
        topic: String,
        host: String,
        port: String,
        ctl: EmissionControl){
        let mut num_current_records = 0;
        let batch_size = cfg.get_batch_size();
        let sender_sema = ctl.get_sender_sema();
        let receiver_sema = ctl.get_receiver_sema();
        let sender = ctl.get_sender();
        let streams = cfg.get_streams();
        let mut emitter = RecordEmitter::new(
            batch_size.clone(), sender_sema, receiver_sema, sender, streams);
        let host_string = format!("{}:{}", host, port);
        let socket_addr = host_string.parse().unwrap();
        let clr = client::pubsub_connect(&socket_addr).await;
        let client = clr.unwrap();
        let msg_result = client.subscribe(topic.as_str()).await;
        let mut msgs = msg_result.unwrap();
        let mut looped_iterations = 0;
        while true {
            let msg_result = tokio::time::timeout(Duration::from_millis(2500), msgs.next()).await;
            if msg_result.is_ok() {
                let msg_opt = msg_result.unwrap();
                if msg_opt.is_some() {
                    let msg_result = msg_opt.unwrap();
                    if msg_result.is_ok() {
                        let msg = msg_result.unwrap();
                        let mp_res = String::from_resp(msg);
                        if mp_res.is_ok() {
                            let mp_str = mp_res.unwrap();
                            let deser_res = serde_json::from_str(&mp_str);
                            if deser_res.is_ok() {
                                num_current_records += 1;
                                let mp: Record = deser_res.unwrap();
                                emitter.publish(mp);
                                if num_current_records == batch_size {
                                    num_current_records = 0;
                                    looped_iterations = 0;
                                    let _r = emitter.emit_records().await;
                                }
                            }
                        }
                    }
                }
            }
            looped_iterations += 1;
            if looped_iterations > 10 && num_current_records > 0{
                let _r = emitter.emit_records().await;
                looped_iterations = 0;
            }
        }
        let _r = emitter.emit_records().await;
    }

    /// Run the redis source.
    pub async fn run(&mut self){
        let ctl = self.control.clone();
        let config = self.config.clone();
        let mut sch = self.signal_channel.clone();
        let connections = config.get_connection_names();
        let credentials: Vec<(String, Credential)> = self.get_credentials(connections);
        let mut handles = vec![];
        for (connection_name, credential) in credentials{
            let topic = config.get_topic(connection_name);
            if topic.is_some() {
                let opts = credential.get_options();
                let host_opt = opts.get("host");
                let port_opt = opts.get("port");
                if host_opt.is_some() && port_opt.is_some() {
                    let host = host_opt.unwrap().clone();
                    let port = port_opt.unwrap().clone();
                    let spawn_config = config.clone();
                    let spawn_ctl = ctl.clone();
                    let handle = tokio::spawn(async move {
                        RedisSource::create_consumer(
                            spawn_config,
                            topic.unwrap(),
                            host,
                            port,
                            spawn_ctl).await;
                    });
                    handles.push(handle);
                }
            }
        }
        for handle in handles{
            let _r = handle.await;
        }
        let _r = sch.send(Signal::SOURCECOMPLETE);
    }

    /// Create a new Redis Source
    pub fn new(
        config: RedisSourceConfig,
        control: EmissionControl,
        signal_channel: Sender<Signal>,
        credentials_db: CredentialDatabase) -> RedisSource{
        RedisSource{
            config,
            control,
            signal_channel,
            credentials_db
        }
    }
}

#[cfg(test)]
pub mod tests{
    use std::sync::Arc;

    use crossbeam::crossbeam_channel::Receiver;
    use redis_async::{client, error};
    use redis_async::resp_array;
    use si_credentials_store::registry::credentials::builder::CredentialDatabaseBuilder;
    use tokio::runtime::{Builder, Runtime};
    use tokio::sync::{mpsc, Notify};

    use crate::control::emission::EmissionControl;
    use crate::sources::redis::RedisSource;
    use crate::stream::flow::source::redis::RedisSourceConfig;
    use crate::stream::stream::Stream;
    use crate::records::record::Record;
    use redis_async::resp::RespValue;
    use std::time::Duration;

    /// Get the credentials information
    fn get_credentials_info() -> (String, String, String){
        let mut input = std::io::stdin();
        let mut db = String::new();
        let mut meta = String::new();
        let mut secret = String::new();
        println!("Credentials DB path:");
        let r = input.read_line(&mut db);
        assert!(r.is_ok());
        println!("Meta path:");
        let r = input.read_line(&mut meta);
        assert!(r.is_ok());
        println!("Seceret Key:");
        let r = input.read_line(&mut secret);
        assert!(r.is_ok());
        (db, meta, secret)
    }

    fn get_runtime() -> Runtime{
        let r = Builder::default()
            .threaded_scheduler()
            .core_threads(4)
            .enable_all()
            .build();
        r.unwrap()
    }

    fn get_emission_control() -> (EmissionControl, Arc<Receiver<(Vec<Record>, Vec<Stream>)>>){
        EmissionControl::new(10)
    }

    #[test]
    fn should_receive_records_from_redis_and_emmit(){
        let mut rt = get_runtime();
        let (db, meta, secret) = get_credentials_info();
        let mut builder = CredentialDatabaseBuilder::default();
        builder.meta_path(meta.clone().trim().to_string());
        builder.fpath(db.trim().to_string());
        builder.verification_path(meta.trim().to_string());
        let db = builder.build(secret.trim().to_string()).unwrap();
        let stream = Stream::new();
        let mut cfg = RedisSourceConfig::new();
        cfg.batch_size(10);
        cfg.add_connection_name("redis1".to_string());
        cfg.add_stream(stream);
        cfg.set_topic_for_connection(
            "redis1".to_string(),
            "test_topic".to_string());
        let (ct, rx) = get_emission_control();
        let (signal_tx, _signal_rx) = mpsc::channel(100);
        let mut src = RedisSource::new(
            cfg,  ct, signal_tx, db);
        let notify = Arc::new(Notify::new());
        let arc_notify = notify.clone();
        let h = rt.spawn(async move {
            arc_notify.notify();
            src.run().await;
        });
        rt.block_on(async move{
            notify.notified().await;
            tokio::time::delay_for(Duration::from_millis(5000)).await;
            let addr = "127.0.0.1:6379".parse().unwrap();
            let conn = client::paired_connect(&addr)
                .await
                .expect("Cannot open connection");
            for _i in 0..12 as i32 {
                let result: Result<RespValue, error::Error>  = conn.send(
                    resp_array!["PUBLISH", "test_topic", "{\"is_ok\": true}"]).await;
            }
            let batch_result = rx.recv();
            let (batch, streams) = batch_result.ok().unwrap();
            assert!(batch.len() == 10);
            let v = batch.get(0);
            assert!(v.is_some());
            let r = v.unwrap();
        });
    }

    #[test]
    fn should_emmit_from_multiple_consumers(){
        let mut rt = get_runtime();
        let (db, meta, secret) = get_credentials_info();
        let mut builder = CredentialDatabaseBuilder::default();
        builder.meta_path(meta.clone().trim().to_string());
        builder.fpath(db.trim().to_string());
        builder.verification_path(meta.trim().to_string());
        let db = builder.build(secret.trim().to_string()).unwrap();
        let stream = Stream::new();
        let mut cfg = RedisSourceConfig::new();
        cfg.batch_size(10);
        cfg.add_connection_name("redis1".to_string());
        cfg.add_connection_name("redis2".to_string());
        cfg.add_stream(stream);
        cfg.set_topic_for_connection(
            "redis1".to_string(),
            "test_topic".to_string());
        cfg.set_topic_for_connection(
            "redis2".to_string(),
            "test_topic2".to_string());
        let (ct, rx) = get_emission_control();
        let (signal_tx, _signal_rx) = mpsc::channel(100);
        let mut src = RedisSource::new(
            cfg,  ct, signal_tx, db);
        let notify = Arc::new(Notify::new());
        let arc_notify = notify.clone();
        let h = rt.spawn(async move {
            arc_notify.notify();
            src.run().await;
        });
        rt.block_on(async move{
            notify.notified().await;
            tokio::time::delay_for(Duration::from_millis(5000)).await;
            let addr = "127.0.0.1:6379".parse().unwrap();
            let conn = client::paired_connect(&addr)
                .await
                .expect("Cannot open connection");
            for _i in 0..10 as i32 {
                let result: Result<RespValue, error::Error>  = conn.send(
                    resp_array!["PUBLISH", "test_topic", "{\"is_ok\": true}"]).await;
            }
            for _i in 0..10 as i32 {
                let result: Result<RespValue, error::Error>  = conn.send(
                    resp_array!["PUBLISH", "test_topic2", "{\"is_ok\": true}"]).await;
            }
            let batch_result = rx.recv();
            let (batch, streams) = batch_result.ok().unwrap();
            assert!(batch.len() == 10);
            let v = batch.get(0);
            assert!(v.is_some());
            let r = v.unwrap();
        });
    }
}
