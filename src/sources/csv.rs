//! Read a csv source. Reads multiple files from a directory. Returns
//! a recod per line.
//!
//! Reads a number of files up to the maximum amount. You shouldn't need to create
//! dozens of sources for different files. Provide directory or file paths through the
//! sources config.
//!
//! The source opens up the `max_open_files` size and reads to individual record emitters.
//! Emmitters emit records to the stream at `batch_size` or on request (when the files are
//! read).
//!
//! Each record has the `line_number` and file `path` added.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use tokio::sync::mpsc::Sender as TokioSender;
use tokio::sync::Mutex;

use crate::control::emission::EmissionControl;
use crate::emitter::records::RecordEmitter;
use crate::file::utilities::get_files;
use crate::monitor::stream::Signal;
use crate::stream::flow::source::csv::CSVSourceConfig;
use si_file_utils::csv::csvreader::CSVReader;
use std::collections::{HashMap, HashSet};
use datacannon_rs_core::argparse::argtype::ArgType;
use crate::records::record::Record;

/// CSV Source
pub struct CSVSource{
    config: CSVSourceConfig,
    control: EmissionControl,
    file_stack: Arc<Mutex<Vec<String>>>,
    signal_channel: TokioSender<Signal>
}


impl CSVSource{

    /// Convert the CSV hshmap to a recod
    ///
    /// # Arguments
    /// * `record` - csv record
    fn convert_record(record: HashMap<String, String>) -> Record{
        let mut output = HashMap::<String, ArgType>::new();
        for (k, v) in record{
            output.insert(k, ArgType::String(v));
        }
        output
    }

    /// Reads csvs
    async fn csv_reader(file_stack: Arc<Mutex<Vec<String>>>,
                        cfg: CSVSourceConfig,
                        ctl: EmissionControl){
        let fstack = file_stack;
        let mut num_current_records = 0;
        let batch_size = cfg.get_batch_size();
        let sender_sema = ctl.get_sender_sema();
        let receiver_sema = ctl.get_receiver_sema();
        let sender = ctl.get_sender();
        let streams = cfg.get_streams();
        let csv_opts= cfg.get_reader_options();
        let mut emitter = RecordEmitter::new(
            batch_size.clone(), sender_sema, receiver_sema, sender, streams);
        let mut rlock = fstack.lock().await;
        let mut f = (*rlock).pop();
        drop(rlock);
        while f.is_some(){
            let mut current_record = 0;
            let path: String = f.unwrap();
            let mut reader = CSVReader::new(
                path.clone(), csv_opts.clone(), None).await;
            let mut record_opt = reader.next().await;
            while record_opt.is_some() {
                let records =  record_opt.unwrap();
                for record in records {
                    let mut stream_record = CSVSource::convert_record(record);
                    stream_record.insert("path".to_string(), ArgType::String(path.clone()));
                    stream_record.insert("record_number".to_string(), ArgType::Int(current_record));
                    current_record += 1;
                    emitter.publish(stream_record);
                    num_current_records += 1;
                    if num_current_records == batch_size{
                        let _r = emitter.emit_records().await;
                        num_current_records = 0;
                    }
                }
                record_opt = reader.next().await;
            }
            reader.close().await;
            rlock = fstack.lock().await;
            f = (*rlock).pop();
            drop(rlock);
        }
        let _r = emitter.emit_records().await;
    }

    /// Run the csv file reader. Find files, create readers, and manage readers.
    pub async fn run(&self){
        let ctl = self.control.clone();
        let config = self.config.clone();
        let fstack = self.file_stack.clone();
        let paths = self.config.get_paths();
        let mut files = get_files(paths);
        files.sort();
        let fset: HashSet<String> = files.drain(..).collect();
        files = fset.into_iter().collect();
        if files.len() > 0 {
            let mut handles = vec![];
            let mut wlock = fstack.lock().await;
            (*wlock).extend(files);
            drop(wlock);
            while handles.len() < config.get_max_open_files(){
                let arc_ctl = ctl.clone();
                let arc_fstack = fstack.clone();
                let reader_cfg = config.clone();
                let handle = tokio::spawn(async move{
                    CSVSource::csv_reader(
                        arc_fstack,
                        reader_cfg,
                        arc_ctl).await;
                });
                handles.push(handle);
            }
            for handle in handles{
                let _r = handle.await;
            }
            let mut sch = self.signal_channel.clone();
            let _r = sch.send(Signal::SOURCECOMPLETE).await;
        }
    }

    /// Create a new CSV Reader
    ///
    /// # Arguments
    /// * `config` - CSV Source configuration
    /// * `emission_control` - Structures controlling record emission
    /// * `signal_channel` - Channel to signal source termination and other operations
    pub fn new(
        config: CSVSourceConfig,
        emission_control: EmissionControl,
        signal_channel: TokioSender<Signal>) -> CSVSource{
        CSVSource{
            config,
            control: emission_control,
            file_stack: Arc::new(Mutex::new(vec![])),
            signal_channel
        }
    }
}

#[cfg(test)]
pub mod tests{
    use crossbeam::crossbeam_channel::Receiver;
    use si_file_utils::csv::csvreader::CSVReaderOptionsBuilder;
    use tokio::runtime::{Builder, Runtime};
    use tokio::sync::mpsc;

    use crate::records::record::Record;
    use crate::stream::stream::Stream;

    use super::*;
    use std::sync::atomic::{AtomicBool, Ordering};

    fn get_test_directory() -> String{
        let dir = env!("test_file_directory");
        let sep = std::path::MAIN_SEPARATOR;
        format!("{}{}{}", dir, sep, "csv")
    }

    /// Get a single file test.txt
    fn get_test_file() -> String{
        let d = get_test_directory();
        let sep = std::path::MAIN_SEPARATOR;
        format!("{}{}{}", d, sep, "test.csv")
    }

    fn get_runtime() -> Runtime{
        let r = Builder::default()
            .threaded_scheduler()
            .core_threads(4)
            .enable_all()
            .build();
        r.unwrap()
    }

    fn get_csv_config() -> CSVSourceConfig{
        let fdir = get_test_directory();
        let mut builder = CSVReaderOptionsBuilder::default();
        builder.headers(vec!{"rid".to_string(), "first_name".to_string(), "last_name".to_string(), "email".to_string(), "gender".to_string(), "ip".to_string()});
        let opts = builder.build().unwrap();
        let mut fsource = CSVSourceConfig::new(opts);
        fsource.add_path(fdir);
        fsource
    }

    fn get_emission_control() -> (EmissionControl, Arc<Receiver<(Vec<Record>, Vec<Stream>)>>){
        EmissionControl::new(10)
    }

    #[test]
    fn should_read_single_csv_to_completion(){
        let mut rt = get_runtime();
        let (sender, _receiver) = mpsc::channel(100);
        let dir = get_test_file();
        let (ec, rx) = get_emission_control();
        let mut fcfg = get_csv_config();
        fcfg.add_path(dir);
        fcfg.batch_size(100);
        fcfg.set_max_open_files(2);
        let fs = CSVSource::new(fcfg, ec.clone(), sender);
        rt.block_on(async move {
            let h = tokio::spawn(async move {
                fs.run().await
            });
            let sema = ec.get_receiver_sema().clone();
            let permit = sema.acquire().await;
            permit.forget();
            let r = rx.recv();
            assert!(r.is_ok());
            let (r, _s) = r.unwrap();
            assert!(r.len() > 0);
            let _r = h.await;
        });
    }

    #[test]
    fn should_read_from_multiple_csv_files(){
        let mut rt = get_runtime();
        let (sender, _receiver) = mpsc::channel(100);
        let dir = get_test_directory();
        let (ec, mut rx) = get_emission_control();
        let mut fcfg = get_csv_config();
        fcfg.add_path(dir);
        fcfg.batch_size(100);
        fcfg.set_max_open_files(2);
        let fs = CSVSource::new(fcfg, ec.clone(), sender);
        let abool = Arc::new(AtomicBool::new(true));
        let arcbool = abool.clone();
        rt.block_on(async move {
            let h = tokio::spawn(async move {
                fs.run().await;
                arcbool.store(false, Ordering::Relaxed);
            });
            for _i in 0..500 {
                if abool.load(Ordering::Relaxed) == false{
                    break;
                }
                let sema = ec.get_receiver_sema().clone();
                let ssema = ec.get_sender_sema().clone();
                let permit = sema.acquire().await;
                permit.forget();
                let r = rx.recv();
                ssema.add_permits(1);
                assert!(r.is_ok());
                let (r, _s) = r.unwrap();
                assert!(r.len() > 0);
            }
            let _r = h.await;
        });
    }

    #[test]
    fn should_send_end_of_stream(){
        let mut rt = get_runtime();
        let (sender, mut receiver) = mpsc::channel(100);
        let dir = get_test_file();
        let (ec, rx) = get_emission_control();
        let mut fcfg = get_csv_config();
        fcfg.add_path(dir);
        fcfg.batch_size(100);
        fcfg.set_max_open_files(2);
        let fs = CSVSource::new(fcfg, ec.clone(), sender);
        let abool = Arc::new(AtomicBool::new(true));
        let arcbool = abool.clone();
        rt.block_on(async move {
            let h = tokio::spawn(async move {
                fs.run().await;
                arcbool.store(false, Ordering::Relaxed);
            });
            for _i in 0..500 as i32 {
                if abool.load(Ordering::Relaxed) == false{
                    break;
                }
                let sema = ec.get_receiver_sema().clone();
                let ssema = ec.get_sender_sema().clone();
                let permit = sema.acquire().await;
                permit.forget();
                let r = rx.recv();
                ssema.add_permits(1);
                assert!(r.is_ok());
                let (r, _s) = r.unwrap();
                assert!(r.len() > 0);
            }
            let signal_opt = receiver.recv().await;
            assert!(signal_opt.is_some());
            let signal = signal_opt.unwrap();
            match signal{
                Signal::SOURCECOMPLETE =>{},
                _ => assert!(false)
            };
            let _r = h.await;
        });
    }
}
