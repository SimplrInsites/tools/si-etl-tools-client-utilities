//! Common Source trait all sources must implement
//!
//! ---
//! author: Andrew Evans
//! ---

use si_credentials_store::registry::credentials::store::CredentialDatabase;

pub trait Source{
    fn close(&mut self);
    fn start(&mut self);
    fn initialize(&mut self, credentials: Option<CredentialDatabase>);
}
