//! Read a jsonl source. Reads multiple files from a directory. Returns
//! a recod per line.
//!
//! Reads a number of files up to the maximum amount. You shouldn't need to create
//! dozens of sources for different files. Provide directory or file paths through the
//! sources config.
//!
//! The source opens up the `max_open_files` size and reads to individual record emitters.
//! Emmitters emit records to the stream at `batch_size` or on request (when the files are
//! read).
//!
//! Each record has the `line_number` and file `path` added.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use tokio::sync::mpsc::Sender as TokioSender;
use tokio::sync::Mutex;

use crate::control::emission::EmissionControl;
use crate::emitter::records::RecordEmitter;
use crate::file::utilities::get_files;
use crate::monitor::stream::Signal;
use crate::stream::flow::source::csv::CSVSourceConfig;
use si_file_utils::csv::csvreader::CSVReader;
use std::collections::{HashMap, HashSet};
use datacannon_rs_core::argparse::argtype::{ArgType, value_to_arg};
use crate::records::record::Record;
use si_file_utils::json::jsonl_reader::JsonlReader;
use crate::stream::flow::source::jsonl::JsonlSourceConfig;
use serde_json::Value;

/// CSV Source
pub struct JsonlSource{
    config: JsonlSourceConfig,
    control: EmissionControl,
    file_stack: Arc<Mutex<Vec<String>>>,
    signal_channel: TokioSender<Signal>
}


impl JsonlSource{

    /// Conver the jsonl map to a record
    ///
    /// # Arguments
    /// * `record` - Appropriately formated hashmap record
    fn convert_to_stream_record(record: HashMap<String, Value>) -> Record{
        let mut output = HashMap::<String, ArgType>::new();
        for (k, v) in record{
            let val = value_to_arg(v).unwrap();
            output.insert(k, val);
        }
        output
    }

    /// Reads csvs
    async fn jsonl_reader(file_stack: Arc<Mutex<Vec<String>>>,
                        cfg: JsonlSourceConfig,
                        ctl: EmissionControl){
        let fstack = file_stack;
        let mut num_current_records = 0;
        let batch_size = cfg.get_batch_size();
        let sender_sema = ctl.get_sender_sema();
        let receiver_sema = ctl.get_receiver_sema();
        let sender = ctl.get_sender();
        let streams = cfg.get_streams();
        let jsonl_opts = cfg.get_reader_options();
        let mut emitter = RecordEmitter::new(
            batch_size.clone(), sender_sema, receiver_sema, sender, streams);
        let mut rlock = fstack.lock().await;
        let mut f = (*rlock).pop();
        drop(rlock);
        while f.is_some(){
            let mut current_record = 0;
            let path: String = f.unwrap();
            let mut reader = JsonlReader::new(
                path.clone(), jsonl_opts.clone(), None).await;
            let mut record_opt = reader.next().await;
            while record_opt.is_some(){
                let records = record_opt.unwrap();
                for record in records{
                    let mut stream_record = JsonlSource::convert_to_stream_record(record);
                    stream_record.insert(
                        "record_number".to_string(), ArgType::Int(current_record));
                    stream_record.insert("path".to_string(), ArgType::String(path.clone()));
                    let _r = emitter.publish(stream_record);
                    current_record += 1;
                    num_current_records += 1;
                    if num_current_records == batch_size{
                        let _r = emitter.emit_records().await;
                        num_current_records = 0;
                    }
                }
                record_opt = reader.next().await;
            }
            reader.close().await;
            rlock = fstack.lock().await;
            f = (*rlock).pop();
            drop(rlock);
        }
        let _r = emitter.emit_records().await;
    }

    /// Run the csv file reader. Find files, create readers, and manage readers.
    pub async fn run(&self){
        let ctl = self.control.clone();
        let config = self.config.clone();
        let fstack = self.file_stack.clone();
        let paths = self.config.get_paths();
        let mut files = get_files(paths);
        files.sort();
        let fset: HashSet<String> = files.drain(..).collect();
        files = fset.into_iter().collect();
        if files.len() > 0 {
            let mut handles = vec![];
            let mut wlock = fstack.lock().await;
            (*wlock).extend(files);
            drop(wlock);
            while handles.len() < config.get_max_open_files(){
                let arc_ctl = ctl.clone();
                let arc_fstack = fstack.clone();
                let reader_cfg = config.clone();
                let handle = tokio::spawn(async move{
                    JsonlSource::jsonl_reader(
                        arc_fstack,
                        reader_cfg,
                        arc_ctl).await;
                });
                handles.push(handle);
            }
            for handle in handles{
                let _r = handle.await;
            }
            let mut sch = self.signal_channel.clone();
            let _r = sch.send(Signal::SOURCECOMPLETE).await;
        }
    }

    /// Create a new CSV Reader
    ///
    /// # Arguments
    /// * `config` - CSV Source configuration
    /// * `emission_control` - Structures controlling record emission
    /// * `signal_channel` - Channel to signal source termination and other operations
    pub fn new(
        config: JsonlSourceConfig,
        emission_control: EmissionControl,
        signal_channel: TokioSender<Signal>) -> JsonlSource{
        JsonlSource{
            config,
            control: emission_control,
            file_stack: Arc::new(Mutex::new(vec![])),
            signal_channel
        }
    }
}

#[cfg(test)]
pub mod tests{
    use crossbeam::crossbeam_channel::Receiver;
    use tokio::runtime::{Builder, Runtime};
    use tokio::sync::mpsc;

    use crate::records::record::Record;
    use crate::stream::stream::Stream;

    use super::*;
    use std::sync::atomic::{AtomicBool, Ordering};
    use si_file_utils::json::jsonl_reader::{JsonlReaderOptions, JsonlReaderOptionsBuilder};

    fn get_test_directory() -> String{
        let dir = env!("test_file_directory");
        let sep = std::path::MAIN_SEPARATOR;
        format!("{}{}{}", dir, sep, "json")
    }

    /// Get a single file test.txt
    fn get_test_file() -> String{
        let d = get_test_directory();
        let sep = std::path::MAIN_SEPARATOR;
        format!("{}{}{}", d, sep, "test.jsonl")
    }

    fn get_runtime() -> Runtime{
        let r = Builder::default()
            .threaded_scheduler()
            .core_threads(4)
            .enable_all()
            .build();
        r.unwrap()
    }

    fn get_jsonl_config() -> JsonlSourceConfig{
        let fdir = get_test_directory();
        let mut builder = JsonlReaderOptionsBuilder::default();
        builder.batch_size(100);
        let opts = builder.build().unwrap();
        let mut fsource = JsonlSourceConfig::new(opts);
        fsource.add_path(fdir);
        fsource
    }

    fn get_emission_control() -> (EmissionControl, Arc<Receiver<(Vec<Record>, Vec<Stream>)>>){
        EmissionControl::new(10)
    }

    #[test]
    fn should_read_single_jsonl_to_completion(){
        let mut rt = get_runtime();
        let (sender, _receiver) = mpsc::channel(100);
        let dir = get_test_file();
        let (ec, rx) = get_emission_control();
        let mut fcfg = get_jsonl_config();
        fcfg.add_path(dir);
        fcfg.batch_size(100);
        fcfg.set_max_open_files(2);
        let fs = JsonlSource::new(fcfg, ec.clone(), sender);
        rt.block_on(async move {
            let h = tokio::spawn(async move {
                fs.run().await
            });
            let sema = ec.get_receiver_sema().clone();
            let permit = sema.acquire().await;
            permit.forget();
            let r = rx.recv();
            assert!(r.is_ok());
            let (r, _s) = r.unwrap();
            assert!(r.len() > 0);
            let _r = h.await;
        });
    }

    #[test]
    fn should_read_from_multiple_jsonl_files(){
        let mut rt = get_runtime();
        let (sender, _receiver) = mpsc::channel(100);
        let dir = get_test_directory();
        let (ec, mut rx) = get_emission_control();
        let mut fcfg = get_jsonl_config();
        fcfg.add_path(dir);
        fcfg.batch_size(100);
        fcfg.set_max_open_files(2);
        let fs = JsonlSource::new(fcfg, ec.clone(), sender);
        let abool = Arc::new(AtomicBool::new(true));
        let arcbool = abool.clone();
        rt.block_on(async move {
            let h = tokio::spawn(async move {
                fs.run().await;
                arcbool.store(false, Ordering::Relaxed);
            });
            for _i in 0..500 {
                if abool.load(Ordering::Relaxed) == false{
                    break;
                }
                let sema = ec.get_receiver_sema().clone();
                let ssema = ec.get_sender_sema().clone();
                let permit = sema.acquire().await;
                permit.forget();
                let r = rx.recv();
                ssema.add_permits(1);
                assert!(r.is_ok());
                let (hms, _s) = r.unwrap();
                assert!(hms.len() > 0);
            }
            let _r = h.await;
        });
    }

    #[test]
    fn should_send_end_of_stream(){
        let mut rt = get_runtime();
        let (sender, mut receiver) = mpsc::channel(100);
        let dir = get_test_file();
        let (ec, rx) = get_emission_control();
        let mut fcfg = get_jsonl_config();
        fcfg.add_path(dir);
        fcfg.batch_size(100);
        fcfg.set_max_open_files(2);
        let fs = JsonlSource::new(fcfg, ec.clone(), sender);
        let abool = Arc::new(AtomicBool::new(true));
        let arcbool = abool.clone();
        rt.block_on(async move {
            let h = tokio::spawn(async move {
                fs.run().await;
                arcbool.store(false, Ordering::Relaxed);
            });
            for _i in 0..500 as i32 {
                if abool.load(Ordering::Relaxed) == false{
                    break;
                }
                let sema = ec.get_receiver_sema().clone();
                let ssema = ec.get_sender_sema().clone();
                let permit = sema.acquire().await;
                permit.forget();
                let r = rx.recv();
                ssema.add_permits(1);
                assert!(r.is_ok());
                let (r, _s) = r.unwrap();
                assert!(r.len() > 0);
            }
            let signal_opt = receiver.recv().await;
            assert!(signal_opt.is_some());
            let signal = signal_opt.unwrap();
            match signal{
                Signal::SOURCECOMPLETE =>{},
                _ => assert!(false)
            };
            let _r = h.await;
        });
    }
}
