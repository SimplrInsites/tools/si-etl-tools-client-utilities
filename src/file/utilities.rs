//! File utilities for reading and using directories
//!
//! ---
//! author: Andrew Evans
//! ---

use std::path::Path;
use std::fs::{metadata, read_dir};

/// Recursively get all files in a directory
///
/// # Arguments
/// * `dir` - Directory to start in
pub fn list_files(dir: String) -> Vec<String>{
    let mut files = vec![];
    let dir_reader = read_dir(dir).unwrap();
    for path in dir_reader{
        if path.is_ok() {
            let entry = path.unwrap();
            if entry.path().is_file(){
                let path = entry.path();
                let pstr = path.to_str().unwrap();
                files.push(pstr.to_string());
            }else{
                let dir = entry.path();
                let dir_str = dir.to_str().unwrap();
                let fvec = list_files(dir_str.to_string());
                files.extend(fvec);
            }
        }
    }
    files
}

/// Get the files from the specified directories
///
/// # Arguments
/// * `paths` - Directories or files to start with
pub fn get_files(paths: Vec<String>) -> Vec<String>{
    let mut files = vec![];
    for path in paths{
        if Path::new(&path).exists() {
            let m = metadata(path.clone()).unwrap();
            if m.is_file() {
                files.push(path);
            }else{
                let mut file_vec = list_files(path);
                files.extend(file_vec);
            }
        }
    }
    files
}


#[cfg(test)]
pub mod test{
    use crate::file::utilities::get_files;
    use std::path::Path;

    #[test]
    fn should_read_directory(){
        let paths = vec!{"E:\\compose".to_string()};
        let files = get_files(paths);
        assert!(files.len() > 0);
        for file in files{
            println!("{}", file.clone());
            let pstr = Path::new(&file);
            assert!(pstr.exists());
            assert!(pstr.is_file());
        }
    }

    #[test]
    fn should_read_file(){
        let opstr = "E:\\compose\\vector\\vector.toml".to_string();
        let paths = vec!{opstr.clone()};
        let mut files = get_files(paths);
        assert!(files.len() == 1);
        let p = files.pop().unwrap();
        let pth = Path::new(&p);
        assert!(pth.exists());
        assert!(pth.is_file());
        let pstr = pth.to_str();
        assert!(pstr.unwrap().eq(&opstr))
    }
}