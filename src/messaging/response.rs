//! Response to the application. Tracsk the number of attempts and includes the response.
//!
//! ---
//! author: Andrew Evans
//! ---

use datacannon_rs_core::task::result::TaskResponse;

/// ETL response also tracking the number of attempts
#[derive(Clone, Debug, Builder)]
pub struct ETLTaskResponse{
    task_response: TaskResponse,
    attempt: u8
}

impl ETLTaskResponse{

    /// Create a new task response
    ///
    /// # Arguments
    /// * `task_response` - Task Response
    pub fn new(task_response: TaskResponse, attempt: u8) -> ETLTaskResponse{
       ETLTaskResponse{
           task_response,
           attempt
       }
    }
}
