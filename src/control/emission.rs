//! Structure storing control variables for record emission. The structure stores
//! semaphores for sending and receiving as well as the sender mpmc channel.
//!
//! Limiting the number of batches in the stream allows batches to process and saves
//! resources.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use crossbeam::crossbeam_channel::{self, Receiver, Sender};
use tokio::sync::Semaphore;

use crate::records::record::Record;
use crate::stream::stream::Stream;

/// Stores control variables
#[derive(Clone)]
pub struct EmissionControl{
    sender_sema: Arc<Semaphore>,
    receiver_sema: Arc<Semaphore>,
    sender: Arc<Sender<(Vec<Record>, Vec<Stream>)>>
}


impl EmissionControl{

    /// Get the receiver semaphore
    pub fn get_receiver_sema(&self) -> Arc<Semaphore>{
        self.receiver_sema.clone()
    }

    /// Get the sender semaphore
    pub fn get_sender_sema(&self) -> Arc<Semaphore>{
        self.sender_sema.clone()
    }

    pub fn get_sender(&self) -> Arc<Sender<(Vec<Record>, Vec<Stream>)>>{
        self.sender.clone()
    }

    /// Create a new control, returning the receiver for the encapsulated sender
    ///
    /// # Arguments
    /// * `permits` - Number of batches allowed in the channel at a time
    pub fn new(
        permits: usize) -> (EmissionControl, Arc<Receiver<(Vec<Record>, Vec<Stream>)>>){
        let (sender, receiver) = crossbeam_channel::bounded(permits);
        let arc_sender = Arc::new(sender);
        let sender_sema = Arc::new(Semaphore::new(permits));
        let receiver_sema = Arc::new(Semaphore::new(0));
        let ctl = EmissionControl{
            sender_sema,
            receiver_sema,
            sender: arc_sender
        };
        (ctl, Arc::new(receiver))
    }
}
